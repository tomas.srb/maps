#!/usr/bin/env python
import re
import base64

def my_open(file_name):
  print('nacitam %s...' % file_name)
  return open(file_name)

def repack_file(input, out):
  print('\nNacitam %s...' % input);
  text = open(input, 'r').read()
  output = text

  output = output.replace('<head>', '<head>\n    <!-- Tenhle soubor nema smysl upravovat, je generovany -->')

  r_script = re.compile(r'<script\s+src="([^"]+)">')
  output = r_script.sub(lambda match: '<script>' + my_open(match.group(1)).read(), output)

  r_image = re.compile(r'url\(([^{)]+)\)')
  output = r_image.sub(lambda match: "url('data:image/png;base64,%s')" % base64.b64encode(my_open(match.group(1)).read()), output)

  print('ukladam templates/%s...' % out);
  open('templates/%s' % out, 'w+').write(output)

repack_file('maps.html', 'maps-all.php');
repack_file('icoedit.html', 'icoedit-all.php');
print('hotovo.');

<?php
  header('HTTP/1.0 503 Service Unavailable');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Dračák</title>
    <style>
      h1, p { text-align: center; }
    </style>
  </head>
  <body>
    <h1>Údržba</h1>
    <p>Prosím, omluvte dočasnou nedostupnost aplikace z důvodu aktualizace systému.</p>
    <h1>System maintenance</h1>
    <p>Please excuse the temporary unavailability of the application due to the system update.</p>
  </body>
</html>

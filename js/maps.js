var boardCells = [];
var xSize = 50;
var ySize = 50;
var selectedFunction = 'floor';
var sampleCell = null;
var currCellCoords = null;
var description = null;
var board = null;
var toolbar = null;
var levels = null;
var xMaxMove = 0;
var yMaxMove = 0;
var xMinMove = xSize;
var yMinMove = ySize;
var currentBoardTimestamp = 0;
var savedMaps = '';
var indexUrl = '';
var readOnly = null;
var touchable = "ontouchend" in document;

function CellUpdateBorder(e1, e2, direction) {
  if (e1.floor && e2.floor) {
    $(e1).addClass(direction+'-no').removeClass(direction+'-edge');
  } else if (e1.floor || e2.floor) {
    $(e1).addClass(direction+'-edge').removeClass(direction+'-no');
  } else if (! e1.floor) {
    $(e1).removeClass(direction+'-no').removeClass(direction+'-edge');
  }
}

function CellUpdateDesign(el) {
  if (el.floor) {
    $(el).addClass('floor');
  } else {
    $(el).removeClass('floor');
  }
  if (el.xPos > 0) {
    CellUpdateBorder(boardCells[el.yPos][el.xPos-1], el, 'right');
  }
  if (el.yPos > 0) {
    CellUpdateBorder(boardCells[el.yPos-1][el.xPos], el, 'bottom');
  }
  if (el.xPos<xSize-1) {
    CellUpdateBorder(el, boardCells[el.yPos][el.xPos+1], 'right');
  }
  if (el.yPos<ySize-1) {
    CellUpdateBorder(el, boardCells[el.yPos+1][el.xPos], 'bottom');
  }
}

function SetCellFloor(el) {
  el.floor = true;
  CellUpdateDesign(el);
}

function RemoveDesignClasses(el, selectedFunction) {
  var classes = $(el).attr('class').split(' ');
  for( var i in classes) {
    if (classes[i] != selectedFunction && classes[i].substr(0,2) == 'd-') {
      $(el).removeClass(classes[i]);
    }
  }
}

function MapCaptionEdit() {
  var caption = $('.caption', this);
  if (! caption.length) {
    caption = $(this).parent().find('.caption');
  }
  caption.text(prompt('Jméno mapy:', caption.text()));
  return false;
}


function LevelCaptionEdit() {
  var caption = $('.caption', this);
  if (! caption.length) {
    caption = $(this).parent().find('.caption');
  }
  caption.text(prompt('Jméno podlaží:', caption.text()));
  return false;
}

function CreateLevelDiv(levelNumber, classes, boardText) {
  var newLevel = document.createElement('div');
  $(newLevel).attr('class', classes).click(LevelClick);
  var caption = document.createElement('span');
  $(caption).addClass('caption').text(levelNumber).appendTo(newLevel);
  if (!readOnly) {
    var buttonEdit = document.createElement('span');
    $(buttonEdit).text('O').addClass('button').attr('title', 'Přejmenovat').click(LevelCaptionEdit).appendTo(newLevel);
    $(newLevel).dblclick(LevelCaptionEdit);
  }
  newLevel.boardText = boardText;
  return newLevel;
}

function FixFirstLevelAdddition() {
  if ($('.level', levels).length == 0) {
    levels.append(CreateLevelDiv(0, 'level selected base', BoardToText()));
  }
}

function GetDesignClasses(dir) {
  var classes = [];
  $('.cell', toolbar).each(function() {
    var design = GetDesignClass(this);
    if (design.substr(-dir.length) == dir) {
      classes.push(design);
    }
  });
  return classes;
}

function CheckStairs(next, currentDir, nextDir) {
  if (!next.length) return;
  var mapParts = next.get(0).boardText.split('|');
  if (parseInt(mapParts[3]) < currentBoardTimestamp) return;
  var reversedCells = {};
  if (mapParts.length > 1) {
    var designs = mapParts[1].split(';');
    for(var designI in designs) {
      var design = designs[designI].split(',');
      if (design[0].substr(-nextDir.length) == nextDir) {
        var designClass = design[0].substr(0, design[0].length-nextDir.length) + currentDir;
        for(var i = 1; i < design.length; i++) {
          var coords = design[i].split(':');
          $(boardCells[parseInt(coords[1], 36)][parseInt(coords[0], 36)]).addClass(designClass).addClass('confirmed');
        }
      }
    }
  }
  var designClasses = GetDesignClasses(currentDir);
  for(var designClassesI in designClasses) {
    var dc = designClasses[designClassesI];
    $('.'+dc, board).not('.confirmed').removeClass(dc);
  }
}

function MakeLevelSelected(level) {
  $(level).addClass('selected');
  TextToBoard(level.boardText);
  CheckStairs($(level).prev(), '-up','-down');
  CheckStairs($(level).next(), '-down', '-up');
}

function LevelClick(event) {
  if ($(this).hasClass('selected')) return;
  $('.selected', levels).removeClass('selected').get(0).boardText = BoardToText();
  MakeLevelSelected(this);
}

function GetLevelNumber() {
  var base;
  var selected;
  var i = 0;
  $('.level', levels).each(function () { if ($(this).hasClass('base')) base = i; if ($(this).hasClass('selected')) selected = i; i++; });
  return base - selected;
}

function GetNewLevelDiv(levelNumber) {
  return CreateLevelDiv(levelNumber, 'level', '||');
}

function ModifyAffectedBoard(cell, affectedLevelElement, oldDirectionLength, direction) {
  var newFunction = selectedFunction.substr(0,selectedFunction.length-oldDirectionLength) + direction;
  if ($('.'+newFunction, toolbar).lenngth == 0) return;
  var designs = affectedLevelElement.boardText.split('|');
  designs[1] += newFunction + ','+cell.xPos.toString(36)+':'+cell.yPos.toString(36);
  affectedLevelElement.boardText = designs.join('|');
}
 
function CellClick() {
  if (selectedFunction.substr(0,2) == 'd-') {
    $(this).toggleClass(selectedFunction);
    RemoveDesignClasses(this, selectedFunction);
    if (selectedFunction.substr(-3) == '-up') {
      if ($(this).hasClass(selectedFunction)) {
        FixFirstLevelAdddition();
        if ($('.selected', levels).prev().length == 0) {
          var affectedLevel = GetNewLevelDiv(GetLevelNumber()+1);
          $('.selected', levels).before(affectedLevel);
        }
      }
    } else if (selectedFunction.substr(-5) == '-down') {
      if ($(this).hasClass(selectedFunction)) {
        FixFirstLevelAdddition();
        if ($('#levels .selected').next().length == 0) {
          var affectedLevel = GetNewLevelDiv(GetLevelNumber()-1);
          $('.selected', levels).after(affectedLevel);
        } 
      }
    }
  }
}

function Eraser(el, force) {
  RemoveDesignClasses(el, '');
  if (force) {
    el.floor = false;
    CellUpdateDesign(el);
  }
}

function CelCopy(x1, y1, x2, y2) {
  if (x2 < 0 || y2 < 0 || x2 >= xSize || y2 >= ySize) return;
  if (x1 < 0 || y1 < 0 || x1 >= xSize || y1 >= ySize) {
    boardCells[y2][x2].floor = false;
    $(boardCells[y2][x2]).attr('class', 'cell');
  } else {
    boardCells[y2][x2].floor = boardCells[y1][x1].floor;
    $(boardCells[y2][x2]).attr('class', $(boardCells[y1][x1]).attr('class'));
  }
  CellUpdateDesign(boardCells[y2][x2]);
}

function MoveAreaRight() {
  if (xMaxMove == xSize-1) return;
  for (var x = xMaxMove; x >= xMinMove-1; x--) {
    for (var y = yMinMove; y <= yMaxMove; y++) {
      if ((x >= 0 && $(boardCells[y][x]).hasClass('to-move')) || (x < xSize-1 && $(boardCells[y][x+1]).hasClass('to-move'))) {
        CelCopy(x, y, x+1, y);
      }
    }
  }
  xMaxMove++;
  xMinMove++;
}

function MoveAreaLeft() {
  if (xMinMove == 0) return;
  for (var x = xMinMove; x <= xMaxMove+1; x++) {
    for (var y = yMinMove; y <= yMaxMove; y++) {
      if ((x < xSize && $(boardCells[y][x]).hasClass('to-move')) || (x > 0 && $(boardCells[y][x-1]).hasClass('to-move'))) {
        CelCopy(x, y, x-1, y);
      }
    }
  }
  xMaxMove--;
  xMinMove--;
}

function MoveAreaDown() {
  if (yMaxMove == ySize-1) return;
  for (var y = yMaxMove; y >= yMinMove-1; y--) {
    for (var x = xMinMove; x <= xMaxMove; x++) {
      if ((y >= 0 && $(boardCells[y][x]).hasClass('to-move')) || (y < ySize-1 && $(boardCells[y+1][x]).hasClass('to-move'))) {
        CelCopy(x, y, x, y+1);
      }
    }
  }
  yMaxMove++;
  yMinMove++;
}

function MoveAreaUp() {
  if (yMinMove == 0) return;
  for (var y = yMinMove; y <= yMaxMove+1; y++) {
    for (var x = xMinMove; x <= xMaxMove; x++) {
      if ((y < ySize && $(boardCells[y][x]).hasClass('to-move')) || (y > 0 && $(boardCells[y-1][x]).hasClass('to-move'))) {
        CelCopy(x, y, x, y-1);
      }
    }
  }
  yMaxMove--;
  yMinMove--;
}

function MoveArea(el) {
  if (el.xPos > sampleCell.xPos) {
    MoveAreaRight();
  } else if (el.xPos < sampleCell.xPos) {
    MoveAreaLeft();
  } else if (el.yPos > sampleCell.yPos) {
    MoveAreaDown();
  } else if (el.yPos < sampleCell.yPos) {
    MoveAreaUp();
  }
  sampleCell = el;
}

function SetFloorCellFromSample() {
  this.floor = sampleCell.floor;
  CellUpdateDesign(this);
}

function SetFloorArea(floor) {
  $('.to-fill', board).removeClass('to-fill').each(SetFloorCellFromSample);
}

function SetSelectArea(x1, y1, x2, y2) {
  $('.to-fill', board).removeClass('to-fill');
  if (x1 > x2) { var a = x2; x2 = x1; x1 = a; }
  if (y1 > y2) { var a = y2; y2 = y1; y1 = a; }
  for(var x = x1; x <= x2; x++) {
    for(var y = y1; y <= y2; y++) {
      $(boardCells[y][x]).addClass('to-fill');
    }
  }
}

function DescriptionFocus() {
  if (!touchable) {
    description.focus();
  }
}

function CellIn(event) {
  var touchable = "ontouchend" in document;
  currCellCoords.text(this.xPos+':'+this.yPos);
  description.val(this.description).get(0).cell = this;
  DescriptionFocus();
  if (sampleCell) {
    if (selectedFunction == 'floor') {  
      if (event.ctrlKey) {
        SetSelectArea(sampleCell.xPos, sampleCell.yPos, this.xPos, this.yPos);
      } else {
        this.floor = sampleCell.floor;
        sampleCell = this;
        CellUpdateDesign(this);
      }
    } else if (selectedFunction == 'eraser') {
      Eraser(this, event.ctrlKey);
    } else if (selectedFunction == 'move') {
      MoveArea(this);
    }
  }
}

function FillRoom(x, y, xDelta, yDelta) {
  x += xDelta;
  y += yDelta;
  if (x < 0 || y < 0 || x >= xSize || y >= ySize || ! boardCells[y][x].floor || $(boardCells[y][x]).hasClass('to-move')) return;
  if ((xDelta && ((y < 1 || !boardCells[y-1][x].floor || !boardCells[y-1][x-xDelta].floor) && (y > ySize-2 || !boardCells[y+1][x].floor || !boardCells[y+1][x-xDelta].floor))) ||
      (yDelta && ((x < 1 || !boardCells[y][x-1].floor || !boardCells[y-yDelta][x-1].floor) && (x > ySize-2 || !boardCells[y][x+1].floor || !boardCells[y-yDelta][x+1].floor)))) return;
  $(boardCells[y][x]).addClass('to-move');
  if (x > xMaxMove) xMaxMove = x;
  if (y > yMaxMove) yMaxMove = y;
  if (x < xMinMove) xMinMove = x;
  if (y < yMinMove) yMinMove = y;
  FillRoom(x, y, 1, 0);
  FillRoom(x, y, -1, 0);
  FillRoom(x, y, 0, 1);
  FillRoom(x, y, 0, -1);
}

function getBoardFloor(x,y,def) {
  if (x < 0 || y < 0 || x >= xSize || y >= ySize) return def;
  return boardCells[y][x].floor;
}

function FillVert(x, y) {
  var yi = y;
  while (getBoardFloor(x, yi, false) && ! getBoardFloor(x-1, yi, false) && ! getBoardFloor(x+1, yi, false) ) {
    $(boardCells[yi][x]).addClass('to-move');
    yi--;
  }
  if (getBoardFloor(x, yi, false) && ( getBoardFloor(x-1, yi, false) != getBoardFloor(x+1, yi, false))) {
    $(boardCells[yi][x]).addClass('to-move');
    yi--;
  }
  if (yi+1 < yMinMove) yMinMove = yi + 1;
  yi = y;
  while (getBoardFloor(x, yi, false) && ! getBoardFloor(x-1, yi, false) && ! getBoardFloor(x+1, yi, false) ) {
    $(boardCells[yi][x]).addClass('to-move');
    yi++;
  }
  if (getBoardFloor(x, yi, false) && ( getBoardFloor(x-1, yi, false) != getBoardFloor(x+1, yi, false))) {
    $(boardCells[yi][x]).addClass('to-move');
    yi++;
  }
  if (yi-1 > yMaxMove) yMaxMove = yi - 1;
  if (x > xMaxMove) xMaxMove = x;
  if (x < xMinMove) xMinMove = x;
}

function FillHoriz(x,y) {
  var xi = x;
  while (getBoardFloor(xi, y, false) && ! getBoardFloor(xi, y-1, false) && ! getBoardFloor(xi, y+1, false) ) {
    $(boardCells[y][xi]).addClass('to-move');
    xi--;
  }
  if (getBoardFloor(xi, y, false) && (getBoardFloor(xi, y-1, false) !=  getBoardFloor(xi, y+1, false))) {
    $(boardCells[y][xi]).addClass('to-move');
    xi--;
  }
  if (xi+1 < xMinMove) xMinMove = xi + 1;
  xi = x;
  while (getBoardFloor(xi, y, false) && ! getBoardFloor(xi, y-1, false) && ! getBoardFloor(xi, y+1, false) ) {
    $(boardCells[y][xi]).addClass('to-move');
    xi++;
  }
  if (getBoardFloor(xi, y, false) && (getBoardFloor(xi, y-1, false) !=  getBoardFloor(xi, y+1, false))) {
    $(boardCells[y][xi]).addClass('to-move');
    xi++;
  }
  if (xi-1 > xMaxMove) xMaxMove = xi - 1;
  if (y > yMaxMove) yMaxMove = y;
  if (yMinMove > y) yMinMove = y;
}

function FillForMove(el, keepPrevious) {
  if (! keepPrevious) {
    xMaxMove = 0;
    yMaxMove = 0;
    xMinMove = xSize;
    yMinMove = ySize;
    $('.to-move', board).removeClass('to-move');
  }
  var x = el.xPos;
  var y = el.yPos;
  if ((getBoardFloor(x, y-1, false) || getBoardFloor(x, y+1, false))  && ! getBoardFloor(x-1, y, false) && ! getBoardFloor(x+1, y, false)) FillVert(x, y)
  else if ((getBoardFloor(x-1, y, false) || getBoardFloor(x+1, y, false)) && ! getBoardFloor(x, y-1, false) && ! getBoardFloor(x, y+1, false)) FillHoriz(x,y)
  else FillRoom(el.xPos, el.yPos, 0, 0);
}

function Annotation(cell) {
  var annotator = $('#annotator').get(0);
  annotator.show();
  $('#annotator-cell-coords', annotator).text(cell.xPos+':'+cell.yPos);
  $('#description-data', annotator).val(cell.description).focus();
}

function CellMouseDown(event) {
  if (event.preventDefault) {
    event.preventDefault();
  }
  if (selectedFunction) {
    sampleCell = this;
  }
  if (selectedFunction == 'annotator') {
    Annotation(this);
  } else if (selectedFunction == 'eraser') {
    Eraser(this, event.ctrlKey);
  } else if (selectedFunction == 'move') {
    var dClass = false;
    if (!event.shiftKey) dClass = GetDesignClass(this);
    if (dClass) {
      $('#cover').height($(document).height()).show();
      CoverMouseMove(event);
      $('#drag').attr('class', 'cell '+dClass).show();
    } else if (this.floor) {
      FillForMove(this, event.shiftKey);
    }
  } else if (selectedFunction == 'floor') {
    this.floor = ! this.floor;
    CellUpdateDesign(this);
  }
}

function CoverMouseMove(event) {
  if (event.preventDefault) {
    event.preventDefault();
  }
  var boardPos = board.offset();
  if (event.pageX > (boardPos.left+9) && event.pageX < (boardPos.left+board.innerWidth()-11) &&
      event.pageY > (boardPos.top+9) && event.pageY < (boardPos.top+board.innerHeight()-11)) {
    $('#drag').css('left', (event.pageX-9)+'px').css('top', (event.pageY-9)+'px');
  }
}

function CoverMouseUp(event) {
  var dClass = GetDesignClass(sampleCell);
  if (! event.ctrlKey) {
    $(sampleCell).removeClass(dClass);
  }
  var boardPos = board.offset();
  var X = Math.round((event.pageX - (boardPos.left+8))/20);
  var Y = Math.round((event.pageY - (boardPos.top+8))/20);
  RemoveDesignClasses(boardCells[Y][X], '');
  $(boardCells[Y][X]).addClass(dClass);
  $('#cover').hide();
  $('#drag').hide();
  sampleCell = null;
  DescriptionFocus();
}

function CellMouseUp(event) {
  if (sampleCell && event.ctrlKey && selectedFunction == 'floor') {
    SetFloorArea();
  }
  sampleCell = null;

  DescriptionFocus();
}

var whitelistTags = {
  'b': 1,
  'i': 1,
  'u': 1,
  's': 1,
  'p': 1,
  'em': 1,
  'br': 1,
  'strike': 1,
  'strong': 1,
  'sub': 1,
  'sup': 1,
  'small': 1,
  'big': 1,
}

function filterTagsWhitelist(tag, tagName) {
  if (tagName in whitelistTags) return tag;
  return '';
}

function filterTags(text) {
  return text.replace(/<\/?([^>]*)>/g, filterTagsWhitelist);
}

function CellOut() {
  var cell = description.get(0).cell;
  UpdateCellDescription(cell, description.val());
}
  
function UpdateCellDescription(cell, description) {
  cell.description = description;
  if (cell.description) {
    if (! $('.no', cell).length) {
      var no = document.createElement('span');
      $(no).text(cell.xPos+':'+cell.yPos).addClass('no');
      $(cell).append(no);
      cell.descriptionRow = document.createElement('tr');
      $(cell.descriptionRow).append($(document.createElement('td')).text(cell.xPos+':'+cell.yPos))
                            .append($(document.createElement('td')));
      cell.descriptionRow.posIndex = cell.yPos * xSize + cell.xPos;
      var placed = false;
      $('table#descriptions tbody tr').each(function () {
        if (! placed && this.posIndex > cell.descriptionRow.posIndex) {
          $(this).before(cell.descriptionRow);
          placed = true;
        }
      });
      if (! placed) {
        $('table#descriptions tbody').append(cell.descriptionRow);
      }
    }
    $('td:eq(1)',cell.descriptionRow).html(filterTags(cell.description));
  } else {
    $('span.no', cell).remove();
    $(cell.descriptionRow).remove();
  }
}

function DescriptionKeyPress(event) {
  if (this.cell.description == '') {
    setTimeout(CellOut,1);
  }
}

function CreateBoard() {
  board = $('div#board');
  var descriptionRow = $('#description-row');
  $(board).hover(function () { descriptionRow.css('visibility', 'visible'); }, 
                 function () { descriptionRow.css('visibility', 'hidden'); } );
  for (var y = 0; y < ySize; y++) {
    boardCells[y] = [];
    for (var x = 0; x < xSize; x++) {
      var cell = document.createElement('div');
      if (!x) $(cell).css('clear','both');
      boardCells[y][x] = cell;
      cell.xPos = x;
      cell.yPos = y;
      cell.floor = false;
      cell.description = '';
      $(cell).attr('class', 'cell').click(CellClick).hover(CellIn, CellOut).mousedown(CellMouseDown).mouseup(CellMouseUp);
      board.append(cell);
    }
  }
}

function GetDesignClass(el) {
  var classes = $(el).attr('class').split(' ');
  for( var i in classes) {
    if (classes[i].substr(0,2) == 'd-') {
      return classes[i];
    }
  }
  return '';
}

function SidebarClick() {
  if (selectedFunction == 'move') {
    $('.to-move', board).removeClass('to-move');
    xMaxMove = 0;
    yMaxMove = 0;
    xMinMove = xSize;
    yMinMove = ySize;
  }  
  $('#sidebar .cell.selected').removeClass('selected');
  $(this).addClass('selected');
  selectedFunction = GetDesignClass(this);
  if (!selectedFunction) {
    if ($(this).hasClass('annotator')) {
      selectedFunction = 'annotator';
    } else if ($(this).hasClass('eraser')) {
      selectedFunction = 'eraser';
    } else if ($(this).hasClass('move')) {
      selectedFunction = 'move';
    } else {
      selectedFunction = 'floor';
    }
  }
  $('#sidebar .help').hide();
  $('#sidebar .help.'+selectedFunction).show();
}

function Now() {
 return new Date().getTime();
}

function BoardToText() {
  var text = '';
  var floor = false;
  var count = 0;
  var designs = {};
  var descriptions = [];
  
  $('#board .cell').each(function () {
    if (this.floor == floor) {
      count++;
    } else {
      text += (text ? ',' : '') + count.toString(36);
      count = 1;
      floor = this.floor;
    }
    var dClass = GetDesignClass(this);
    if (dClass) {
      if (! designs[dClass]) designs[dClass] = [];
      designs[dClass].push(this);
    }
    if (this.description) {
      descriptions.push(this.xPos.toString(36)+':'+this.yPos.toString(36)+'@'+encodeURIComponent(this.description));
    }
  });
  if (count && floor) {
    text += (text ? ',' : '') + count.toString(36);
  }
  text += '|';
  for (var d in designs) {
    text += d;
    for (var c in designs[d]) {
      text += ',' + designs[d][c].xPos.toString(36)+':'+designs[d][c].yPos.toString(36);
    }
    text += ';';
  }
  text += '|'+descriptions.join(';')+'|'+Now();
  return text;
}

function ClearBoard() {
  $('#board .cell').attr('class', 'cell').each(function () { this.floor = false; this.description = ''; });
  $('#board .cell .no').remove();
  $('#descriptions tbody tr').remove();
}

function FillFloors(floors) {
  var floor = false;
  var cell = $('#board .cell').first();
  for (var i in floors) {
    var count = parseInt(floors[i],36);
    while (count > 0) {
      count--;
      if (floor) {
        SetCellFloor(cell.get(0));
      }
      cell = cell.next();
    }
    floor = ! floor;
  }
}

function FillDesignClasses(dClasses) {
  for (var sdClass in dClasses) {
    var adClass = dClasses[sdClass].split(',');
    var dClass = adClass[0];
    for (var i = 1; i < adClass.length; i++) {
      if (adClass[i]) {
        var coords = adClass[i].split(':');
        $(boardCells[parseInt(coords[1],36)][parseInt(coords[0],36)]).addClass(dClass);
      }
    }
  } 
}

function FillDescriptions(descriptions) {
  for (var descript in descriptions) {
    var parts = descriptions[descript].split('@');
    if (parts.length > 1) {
      var coords = parts[0].split(':');
      var text = parts[1];
      description.get(0).cell = boardCells[parseInt(coords[1],36)][parseInt(coords[0],36)];
      description.val(decodeURIComponent(text));
      CellOut();
    }
  }
}

function TextToBoard(text) {
  ClearBoard();
  if (! text) return;
  var parts = text.split('|');
  FillFloors(parts[0].split(','));
  FillDesignClasses(parts[1].split(';'));
  FillDescriptions(parts[2].split(';'));
  currentBoardTimestamp = parseInt(parts[3]);
}

function LoadClick() {
  TextToMaps($('#data').val());
  $('#popup').hide();
  $('#cover2').removeClass('shadow');
}

function ClearLevels() {
  $(levels).empty();
}

function TextToMaps(text) {
  ClearLevels();
  var textAndName = text.split('$');
  text = textAndName[0];
  if (1 in textAndName) {
    $('#mapname .caption').text(decodeURIComponent(textAndName[1]));
  }
  if (text && text[0] == '#') {
    var maps = text.split('#');
    for(var mapIndex in maps) {
      var mapParts = maps[mapIndex].split('/');
      if (mapParts.length > 1) {
        var mapDescription = mapParts[1].split('|');
        levels.append(CreateLevelDiv(decodeURIComponent(mapDescription[0]), 'level' + (parseInt(mapDescription[1]) ? ' base' : '') + (parseInt(mapDescription[2]) ? ' selected' : ''), mapParts[0]));
        if (parseInt(mapDescription[2])) {
          TextToBoard(mapParts[0]);
        }
      }
    }
  } else {
    TextToBoard(text);
  }
}

function MapsToText() {
  var levelItems = $('.level', levels);
  var output = '';
  if (levelItems.length) {
    $('.selected', levels).get(0).boardText = BoardToText();
    levelItems.each(function () {
      output += '#'+this.boardText+'/'+encodeURIComponent($('.caption', this).text())+'|'+$(this).hasClass('base')*1+'|'+$(this).hasClass('selected')*1;
    });
  } else {
    output = BoardToText();
  }
  output += '$' + encodeURIComponent($('#mapname .caption').text());
  return output;
}

function InitSidebar() {
  currCellCoords = $('#curr-cell-coords');
  description = $('#description');
  levels = $('#levels');
  toolbar = $('#toolbar');
  if (readOnly) {
    $('.cell', toolbar).remove();
    selectedFunction = '';
    description.attr('readonly', 'readonly');
  } else {
    $('.cell', toolbar).click(SidebarClick);
  };
  $('#description-row').css('visibility', 'hidden');
  description.keypress(DescriptionKeyPress);
}

function InitCover() {
  $('#cover').mousemove(CoverMouseMove).mouseup(CoverMouseUp);
  var cover2 = $('#cover2');
  var cover = cover2.get(0);
  var hidden = false;
  var timeout = null;
  cover2.dragItem = null;
  cover2.mouseup(function () {
    this.dragItem = null;
  }).mousemove(function(event) {
     var dragItem = this.dragItem;
     if (dragItem) {
       if (dragItem.hidden) {
         clearTimeout(timeout);
         timeout = setTimeout(function() { dragItem.hidden = false; dragItem.content.css('visibility', 'visible'); }, 200);
       } else {
         dragItem.hidden = true;
         dragItem.content.css('visibility', 'hidden');
         timeout = setTimeout(function() { dragItem.hidden = false; dragItem.content.css('visibility', 'visible'); }, 200);
       }
       event.preventDefault();
       $(dragItem).css('left', event.pageX-$(dragItem).width()/2+'px');
       $(dragItem).css('top', event.pageY-12+'px');
       return false;
    }
  });
  $('.dialog', cover).each(function () {
    var dialog = this;
    var content = $('.content', dialog);
    dialog.show = function() { $(cover).addClass('shadow').height($(document).height()); $(dialog).center().show(); };
    $('.closer', dialog).click(function() { $(dialog).hide(); $(cover).removeClass('shadow'); });
    $('.title', dialog).mousedown(function(event) {
      cover.dragItem = dialog;
      cover.dragItem.content = content;
      return false;
    });
  });
  
}

function InitPopup() {
  var descriptionData = $('#description-data');
  var annotatorCloser = $('#annotator .closer');
  descriptionData.keyup(function(event) { 
    if (event.keyCode == 13 && event.ctrlKey) {
      annotatorCloser.click();
    }
  });
  annotatorCloser.click(function() {
    UpdateCellDescription(sampleCell, descriptionData.val());
  });
  if (readOnly) {
    $('#load-button').remove();
  } else {
    $('#load-button').click(LoadClick);
  }
}

function TimedLocalSave() {
  window.localStorage['maps_last'] = MapsToText();
}

function InitLocalSave() {
  if ('maps_last' in window.localStorage) {
    TextToMaps(window.localStorage['maps_last']);
  }
  setInterval(TimedLocalSave, 60000);
}

function InitSave() {
  if (window.location.protocol == 'file:') {
    InitLocalSave();
  }
}

function RemoveTimeStampsFromMaps(maps) {
  return maps.replace(/\|[0-9]*[/$]/g, '|/');
}

function MapsChanged(maps) {
  return RemoveTimeStampsFromMaps(savedMaps) != RemoveTimeStampsFromMaps(maps);
}

function SubmitCommand() {
  $('#cover2').addClass('shadow').height($(document).height());
  $('#command-form').submit();
}

function NewMap() {
  $('#command1').attr('name', 'newmap').val('newmap');
  $('#command-form').attr('action', indexUrl);
  SubmitCommand();
}

function NewMapWithSave() {
  $('#command2').attr('name', 'savemap').val('savemap');
  $('#parameter1').attr('name', 'mapdata').val(MapsToText());
  NewMap();
}

function LoadMap() {
  $('#command1').attr('name', 'loadmap').val('loadmap');
  SubmitCommand();
}

function LoadMapWithSave() {
  $('#command2').attr('name', 'savemap').val('savemap');
  $('#parameter1').attr('name', 'mapdata').val(MapsToText());
  LoadMap();
}

function SaveMap() {
  $('#command2').attr('name', 'savemap').val('savemap');
  $('#parameter1').attr('name', 'mapdata').val(MapsToText());
  SubmitCommand();
}

function CopyPaste() {
  $('#data').val(MapsToText()).focus();
  $('#popup').get(0).show();
}

function InitIndexUrl() {
  var cookies = document.cookie.split(';');
  for(var ci in cookies) {
    var cookie = cookies[ci].split('=');
    if (cookie[0].replace(/^\s+|\s+$/g, '') == 'indexurl') {
      indexUrl = cookie[1].replace(/^\s+|\s+$/g, '');
    }
  }
}

function NewIcon() {
  $('#icoedit').get(0).show();
}

function MenuMouseEnter() {
  $('#menu', this).toggleClass('needsave', MapsChanged(MapsToText())).show();
}

function InitMenu() {
  var menu = $('#menu-button');
  menu.hover(MenuMouseEnter, function() { $('#menu', this).hide(); });
  if (! indexUrl) {
    $('.userneeded', menu).remove();
  }
  if (readOnly) {
    $('#save').remove();
  }
  if (window.location.protocol == 'file:') {
    $('#menu .online').remove();
  } else {
    $('#newmap,#newmap-withoutsave').click(NewMap);
    $('#newmap-withsave').click(NewMapWithSave);
    $('#loadmap,#loadmap-withoutsave').click(LoadMap);
    $('#loadmap-withsave').click(LoadMapWithSave);
    $('#savemap').click(SaveMap);
  }
  $('#copypaste').click(CopyPaste);
  $('#newicon').click(NewIcon);
  var part = 'acak;bluepro;dr;cz'.split(';');
  $('#kontakt').attr('href', 'mailto:'+part[2]+part[0]+'@'+part[1]+'.'+part[3]).text(part[2]+part[0]+'@'+part[1]+'.'+part[3]);
}

function LoadEmbededMaps(maps) {
  if (maps && maps[0] != '<') {
    savedMaps = maps;
    TextToMaps(maps);
  }
}

function InitReadOnly(keyType) {
  readOnly = keyType && keyType[0] == 'r';
  $('body').toggleClass('readonly', readOnly).toggleClass('readwrite', !readOnly);
}

function Init() {
  $('#mapname').dblclick(MapCaptionEdit).find('.button').click(MapCaptionEdit);
  InitReadOnly('<?= $keyType ?>');
  InitIndexUrl();
  InitSidebar();
  CreateBoard();
  InitCover();
  InitPopup();
  InitSave();
  InitMenu();
  LoadEmbededMaps('<?= $startMap ?>');
}

$(document).ready(Init);
    

var icoBoard = null;
var iconContexts = [];
var icoBoardCells = [];
var icoLastCell = null;

function UpdateCellState(cell, aFilled) {
  if (cell.filled == aFilled) return;
  cell.filled = aFilled == '!' ? ! cell.filled : aFilled;
  $(cell).toggleClass('filled', cell.filled);
  for(var i in iconContexts) {
    var imageData = iconContexts[i].getImageData(cell.xPos-1, cell.yPos-1, 1, 1);
    imageData.data[3] = cell.filled ? 255 : 0;
    iconContexts[i].putImageData(imageData, cell.xPos-1, cell.yPos-1);
  }
}

function SetLastCell(cell) {
  $(icoLastCell).removeClass('last-cell');
  icoLastCell = cell;
  $(cell).addClass('last-cell');
  $('#ico-mode').toggleClass('mode-draw', icoLastCell.filled);
}

function IcoCellMouseDown(event) {
  if (event.preventDefault) {
    event.preventDefault();
  }
  var pen = (event.shiftKey || event.ctrlKey) ? icoLastCell.filled : '!';
  if (this.xPos == 0 && this.yPos > 0) {
    $('.y'+this.yPos, icoBoard).not('.x0').each(function() { UpdateCellState(this, pen); });
  } else
  if (this.yPos == 0 && this.xPos > 0) {
    $('.x'+this.xPos, icoBoard).not('.y0').each(function() { UpdateCellState(this, pen); });
  } else if (this.xPos && this.yPos) {
    if (event.ctrlKey) {
      if (event.shiftKey) {
        DrawCircle(icoLastCell.xPos, icoLastCell.yPos, this.xPos, this.yPos, plotFinal);
      } else {
        DrawBox(icoLastCell.xPos, icoLastCell.yPos, this.xPos, this.yPos, plotFinal);
        SetLastCell(this);
      }
      IcoCellOut(event);
    } else if (event.shiftKey) {
      DrawLine(icoLastCell.xPos, icoLastCell.yPos, this.xPos, this.yPos, plotFinal);
      SetLastCell(this);
      IcoCellOut(event);
    } else {
      UpdateCellState(this, ! this.filled);
      SetLastCell(this);
    }
  }
}

function DrawLine(x0, y0, x1, y1, plot) {
   var dx = Math.abs(x1-x0);
   var dy = Math.abs(y1-y0) ;
   var sx = x0 < x1 ? 1 : -1;
   var sy = y0 < y1 ? 1 : -1;
   var err = dx-dy;
 
   while (true) {
     plot(x0,y0);
     if (x0 == x1 && y0 == y1) return;
     var e2 = 2 * err;
     if (e2 > -dy) { 
       err -= dy;
       x0 += sx;
     }
     if (e2 <  dx) { 
       err += dx;
       y0 += sy;
     }
   }
}

function DrawCircle(x0, y0, x1, y1, plot) {
  var r = Math.round(Math.sqrt((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0)));
  var y=r;
  var d = -r;
  plot(x0,y0+r);
  plot(x0,y0-r);
  plot(x0+r,y0);
  plot(x0-r,y0);
  for(var x=1;x<r/Math.sqrt(2);x++) {  
    d+= 2*x-1;
    if (d>=0) {
      y--;
      d -= 2*y;
    }
    plot(x0+x,y0+y);
    plot(x0+x,y0-y);
    plot(x0-x,y0-y);
    plot(x0-x,y0+y);
    plot(x0+y,y0+x);
    plot(x0+y,y0-x);
    plot(x0-y,y0-x);
    plot(x0-y,y0+x);
  }
}

function DrawBox(x0, y0, x1, y1, plot) {
  var dx = x1 > x0 ? 1 : -1;
  var dy = y1 > y0 ? 1 : -1;
  for(var y = y0; y != y1+dy; y += dy) {
    for(var x = x0; x != x1+dx; x += dx) {
      plot(x,y);
    }
  }
}

function plotHover(x, y) {
  if (x < 1 || x > 18 || y < 1 || y > 18) return;
  $(icoBoardCells[y][x]).addClass('hover');
}

function plotFinal(x, y) {
  if (x < 1 || x > 18 || y < 1 || y > 18) return;
  var cell = icoBoardCells[y][x];
  if (cell.filled != icoLastCell.filled) {
    UpdateCellState(cell, icoLastCell.filled);
  }
}

function IcoCellIn(event) {
  DrawHover(event, this);
}

function DrawHover(event, element) {
  if (! (element.xPos || element.yPos)) return;
  if (element.xPos == 0) {
    $('.y'+ element.yPos, icoBoard).addClass('hover');
  } else if (element.yPos == 0) {
    $('.x'+ element.xPos, icoBoard).addClass('hover');
  } else if (event.ctrlKey) {
    if (event.shiftKey) {
      DrawCircle(icoLastCell.xPos, icoLastCell.yPos, element.xPos, element.yPos, plotHover);
    } else {
      DrawBox(icoLastCell.xPos, icoLastCell.yPos, element.xPos, element.yPos, plotHover);
    }
  } else if (event.shiftKey) {
    DrawLine(icoLastCell.xPos, icoLastCell.yPos, element.xPos, element.yPos, plotHover);
  }
}

function IcoCellOut(event) {
  $('.hover', icoBoard).removeClass('hover');
}

function CreateBoardCell(x, y) {
  var cell = document.createElement('td');
  icoBoardCells[y][x] = cell;
  cell.xPos = x;
  cell.yPos = y;
  cell.filled = false;
  cell = $(cell);
  cell.attr('class', 'x'+x+' '+'y'+y);
  if (y == 0 && x != 0) {
    cell.text(x);
  }
  if (y != 0 && x == 0) {
    cell.text(y);
  }
  cell.mousedown(IcoCellMouseDown).hover(IcoCellIn, IcoCellOut);
  return cell;
}

function CreateBoardRow(y) {
  icoBoardCells[y] = [];
  var row = $(document.createElement('tr'));
  for(var x = 0; x <= 18; x++) {
    row.append(CreateBoardCell(x, y));
  }
  return row;
}

function InitBoard() {
  icoBoard = $('#ico-board');
  for(var y = 0; y <= 18; y++) {
    icoBoard.append(CreateBoardRow(y));
  }
  SetLastCell(icoBoardCells[1][1]);
}

function InitIcon() {
  $('#ico-samples canvas').each(function () {
    iconContexts.push(this.getContext('2d'));
  });
}

function MoveLeft() {
  for(var x = 1; x < 18 ; x++) {
    for(var y = 1; y < 19; y++) {
      UpdateCellState(icoBoardCells[y][x], icoBoardCells[y][x+1].filled);
    }
  }
  for(var y = 1; y < 19; y++) {
    UpdateCellState(icoBoardCells[y][18], false);
  }
}

function MoveUp() {
  for(var y = 1; y < 18 ; y++) {
    for(var x = 1; x < 19; x++) {
      UpdateCellState(icoBoardCells[y][x], icoBoardCells[y+1][x].filled);
    }
  }
  for(var x = 1; x < 19; x++) {
    UpdateCellState(icoBoardCells[18][x], false);
  }
}

function MoveDown() {
  for(var y = 18; y > 1 ; y--) {
    for(var x = 1; x < 19; x++) {
      UpdateCellState(icoBoardCells[y][x], icoBoardCells[y-1][x].filled);
    }
  }
  for(var x = 1; x < 19; x++) {
    UpdateCellState(icoBoardCells[1][x], false);
  }
}


function MoveRight() {
  for(var x = 18; x > 1 ; x--) {
    for(var y = 1; y < 19; y++) {
      UpdateCellState(icoBoardCells[y][x], icoBoardCells[y][x-1].filled);
    }
  }
  for(var y = 1; y < 19; y++) {
    UpdateCellState(icoBoardCells[y][1], false);
  }
}

function UpdateStatusByKey(event) {
  var pixel = $('#ico-draw-pixel.selected').length;
  $('#ico-mode').toggleClass('pixel', pixel > 0).toggleClass('primitiv', pixel == 0);
  $('.hover', icoBoard).removeClass('hover');
  $('td:hover', icoBoard).each(function () {
    DrawHover(event, this); 
  });
}

function DocumentKeyDown(event) {
  $('#ico-tools .selected').removeClass('selected');
  if (event.ctrlKey) {
    if (event.shiftKey) {
      $('#ico-draw-circle').addClass('selected');
    } else {
      $('#ico-draw-box').addClass('selected');
    }
  } else if (event.shiftKey) {
    $('#ico-draw-line').addClass('selected');
  } else {
    $('#ico-draw-pixel').addClass('selected');
  }
  UpdateStatusByKey(event);
}

function DocumentKeyUp(event) {
  if (event.which == 16) {
    $('#ico-draw-line').removeClass('selected');
    $('#ico-draw-circle').removeClass('selected');
    if (event.ctrlKey) {
      $('#ico-draw-box').addClass('selected');
    }
    event.shiftKey = false;
  } else if (event.which == 17) {
    $('#ico-draw-circle').removeClass('selected');
    $('#ico-draw-box').removeClass('selected');
    if (event.shiftKey) {
      $('#ico-draw-line').addClass('selected');
    }
    event.ctrlKey = false;
  }
  if (! $('#ico-tools .selected').length) {
    $('#ico-draw-pixel').addClass('selected');
    IcoCellOut(event);
  }
  UpdateStatusByKey(event);
}

function InitTools() {
  $('#ico-move-left').click(MoveLeft);
  $('#ico-move-up').click(MoveUp);
  $('#ico-move-down').click(MoveDown);
  $('#ico-move-right').click(MoveRight);
  $(document).keydown(DocumentKeyDown).keyup(DocumentKeyUp);
}

function SaveIcon() {
  var imageData = iconContexts[0].toDataUrl();
  var iconName = $('#ico-name').val();
  $.ajax({
      url: 'mapslist.html',
      type: 'POST',
      data: {
          icondata: imageData,
          iconname: iconName,
          iconid: '',
          saveicon: 'save'
           }
  });
}

function InitActions() {
  $('#saveicon').click(SaveIcon);
}

function IcoInit() {
  InitBoard();
  InitIcon();
  InitTools();
  InitActions();
}

$(document).ready(IcoInit);

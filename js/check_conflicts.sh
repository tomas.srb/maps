grep 'function [A-Z]' maps.js | sed 's/function \(.*\)(.*$/\1/' | sort > maps.lst
grep 'function [A-Z]' icoedit.js | sed 's/function \(.*\)(.*$/\1/' | sort > icoedit.lst
meld maps.lst icoedit.lst

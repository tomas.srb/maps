drop table if exists keys_store;
create table keys_store (
  keys_id binary(32) NOT NULL,
  primary key (keys_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

drop table if exists users;
create table users (
  users_id int(11) NOT NULL auto_increment,
  users_key binary(32) NOT NULL,
  username varchar(255) DEFAULT NULL,
  password binary(36) DEFAULT NULL,
  primary key (users_id),
  unique key (users_key),
  unique key (username),
  CONSTRAINT FOREIGN KEY (users_key) REFERENCES keys_store (keys_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

drop table if exists maps;
create table maps (
  maps_id int(11) NOT NULL auto_increment,
  users_id int(11) NOT NULL,
  rw_key binary(32) NOT NULL,
  ro_key binary(32) NOT NULL,
  created datetime,
  modified datetime,
  name varchar(255),
  map longtext,
  primary key (maps_id),
  unique key (rw_key),
  unique key (ro_key),
  key (users_id),
  CONSTRAINT FOREIGN KEY (users_id) REFERENCES users (users_id),
  CONSTRAINT FOREIGN KEY (rw_key) REFERENCES keys_store (keys_id),
  CONSTRAINT FOREIGN KEY (ro_key) REFERENCES keys_store (keys_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

drop table if exists icons;
create table icons (
  icons_id int(11) NOT NULL auto_increment,
  users_id int(11) NOT NULL,
  name varchar(255),
  class varchar(255),
  picture longblob,
  allowed_public boolean,
  is_public boolean,
  primary key (icons_id),
  key (users_id),
  key (is_public),
  CONSTRAINT FOREIGN KEY (users_id) REFERENCES users (users_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;  

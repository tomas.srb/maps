<?php

define('PROJECT', dirname(__FILE__).'/');
define('LIBS', PROJECT.'libs/');
define('TEMPLATES', PROJECT.'templates/');


require(PROJECT.'config.php');

function MySqlQuery($query, $db) {
  global $DEBUG;
  $res = mysql_query($query, $db);
  if (!$res) {
    error_log($query);
    error_log(mysql_error());
  } elseif ($DEBUG) {
    error_log('debug >> '.$query);
    error_log('Affected: '.mysql_affected_rows($db).'  Returned: '.(is_resource($res) ? mysql_num_rows($res) : 0));
  }
  return $res;
}

function initDatabase($dbConfig) {
  $connection = mysql_connect($dbConfig['host'], $dbConfig['user'], $dbConfig['password']) or die('Error connecting to mysql');
  mysql_select_db($dbConfig['name']);
  MySqlQuery('set names utf8', $connection);
  return $connection;
}

function generateUniqueKey($db, $salt) {
  $retry = 5;
  while ($retry) {
    $key = md5($_SERVER['REMOTE_ADDR'].'/'.$_SERVER['REMOTE_PORT'].'/'.microtime().'/'.$salt);
    $query = "INSERT keys_store (keys_id) VALUES ('$key')";
    $ret = MySqlQuery($query, $db);
    if ($ret) return $key;
    $retry--;
  }
  return false;
}

function storeNewMap($userId, $mapText, $mapName, $db) {
  $rwKey = generateUniqueKey($db, 'w');
  $roKey = generateUniqueKey($db, 'r');
  $query = "INSERT maps (created, modified, users_id, rw_key, ro_key, name, map) VALUES (NOW(), NOW(), $userId, '$rwKey', '$roKey', '$mapName', '$mapText')";
  $ret = MySqlQuery($query, $db);
  if (!$ret) return false;
  return array('rwKey' => $rwKey, 'roKey' => $roKey);
}

function storeNewUser($db) {
  $key = generateUniqueKey($db, 'u');
  $query = "INSERT users (users_key) VALUES ('$key')";
  $ret = MySqlQuery($query, $db);
  if (!$ret) return false;
  $userId = mysql_insert_id($db);
  return array('key' => $key, 'id' => $userId);
}

function generateNewAccount($db) {
  $user = storeNewUser($db);
  if (!$user) return false;
  $sampleMap = '#1z,1,1d,1,1d,1,1a,7,17,7,11,d,11,1,5,7,11,1,a,1,12,1,a,1,10,4,9,1,10,f,z,4,8,3,z,4,8,3,11,1,a,1,12,1,a,1,12,1,a,1,12,1,a,2,11,1,1d,1,1a,7,17,7,17,7,17,9,15,7,17,7,17,7,17,7|d-entry,l:1;d-door1,h:6;d-door2,n:a;d-monster,b:b,c:b,n:c;d-door1s,e:b,l:b,g:n;d-stairs-down,o:h,h:n;d-column,a:l,e:l,c:n,a:p,e:p;|c:c@Hejno%20zu%C5%99iv%C3%BDch%20goblin%C5%AF;n:d@Star%C3%BD%20goblin-%C5%A1aman;c:r@Tady%20by%20mohl%20b%C3%BDt%20poklad|1320740037394/P%C5%99%C3%ADzem%C3%AD|1|1#oa,2,1d,1,18,4,1,1,1,4,13,b,18,1,18,b,10,7,1,1,1,4,18,1,1c,3,1b,3,1b,3|d-stairs-up,o:h,h:n;d-monster,k:k,t:n,p:q;d-door1,o:k,q:k,o:m,q:m;d-door1s,j:n;|p:l@V%C4%9Bzen%C3%AD%20-%20m%C5%99%C3%AD%C5%BEovan%C3%A9%20dve%C5%99e%20po%20stran%C3%A1ch%20vedou%20do%20cel;p:r@M%C3%ADstnost%20pro%20dozorce|1320185474168/Podzemn%C3%AD%20v%C4%9Bzen%C3%AD|0|0$Uk%C3%A1zkov%C3%A1%20mapa';
  storeNewMap($user['id'], $sampleMap, 'Ukázková mapa', $db);
  return $user['key'];
}

function getScriptDirname($scriptName) {
  $dirname = dirname($scriptName);
  return $dirname == '/' ? '' : $dirname;
}

function parseRequestUri($requestUri, $scriptName) {
  $regexp = '#^' . getScriptDirname($scriptName) . '(/(([urw][0-9a-f]{32}|mapslist)\.html)|/?)$#';
  if (!preg_match($regexp, $requestUri, $results)) return false;
  if (isset($results[3]) && $results[3]) {
    return $results[3];
  } else return true;
}

function parseIndexUrl($indexUrl) {
  $regexp = '/^u([0-9a-f]{32})\.html$/';
  if (!preg_match($regexp, $indexUrl, $results)) return false;
  return $results[1];
}

function findUserByKey($key, $db) {
  $query = "SELECT users_id FROM users WHERE users_key = '$key'";
  $res = MySqlQuery($query, $db);
  if (! $res) return false;
  if (!($row = mysql_fetch_assoc($res))) return false;
  return $row['users_id'];
}

function findUserParamsByKey($key, $db) {
  $query = "SELECT users_id, username, password FROM users WHERE users_key = '$key'";
  $res = MySqlQuery($query, $db);
  if (! $res) return false;
  return mysql_fetch_assoc($res);
}

function findUserParamsByUsername($username, $db) {
  $username = mysql_real_escape_string($username);
  $query = "SELECT users_id, username, password, users_key FROM users WHERE username = '$username'";
  $res = MySqlQuery($query, $db);
  if (! $res) return false;
  return mysql_fetch_assoc($res);
}

function encodePassword($salt, $password) {
  return $salt . md5("$salt:$password");
}

function registerUser($userId, $username, $password, $db) {
  if (! $username) {
    return 'Nemá smysl mačkat tlačítko "Registrovat", když nezadáte uživatelské jméno.';
  }
  $username = mysql_real_escape_string($username);
  $salt = substr(sprintf('%04x', rand()), 0, 4);
  $password = encodePassword($salt, $password);
  $query = "UPDATE users SET username = '$username', password = '$password' WHERE users_id = $userId";
  $res = MySqlQuery($query, $db);
  if (!$res) {
    return 'Uživatelské jméno nelze použít, zkuste jiné.';
  }
  return 'Uživatelské údaje zapsány.';
}

function checkPassword($storedPassword, $givenPassword) {
  return $storedPassword == encodePassword(substr($storedPassword, 0, 4), $givenPassword);
}

function loginUser($db) {
  if (! isset($_COOKIE['testcookie'])) {
    welcomePage('Přihlášení vyžaduje v prohlížeči povolené cookies');
  } else {
    $userParams = findUserParamsByUsername($_POST['username'], $db);
    if (! $userParams || ! checkPassword($userParams['password'], $_POST['password'])) {
      welcomePage('K zadaným údajům nebyly nalezeny žádné mapy');
    } else {
      redirectToMapsList($userParams['users_key']);
    }
  }
}

function removeMap($mapId, $userId, $db) {
  $query = "DELETE FROM maps WHERE users_id = $userId AND maps_id = $mapId";
  if (MySqlQuery($query, $db) && mysql_affected_rows($db)) {
    return 'Mapa byla úspěšně odstraněna';
  }
  return 'Mapu se nepodarilo odstranit';
}

function renderMapsList($key, $db) {
  $userParams = findUserParamsByKey($key, $db);
  if (! $userParams) return false;
  $userId = $userParams['users_id'];
  $message = '';
  $tableMessage = '';
  if (isset($_POST['removemap'])) {
    $tableMessage = removeMap((int)$_POST['removemap'], $userId, $db);
  } else if (isset($_POST['register'])) {
    if (! $userParams['username'] || checkPassword($userParams['password'], $_POST['oldpassword'])) {
      $message = registerUser($userId, $_POST['username'], $_POST['password'], $db);
    } else {
      $message = 'Údaje nelze uložit, protože původní heslo bylo zadáno chybně.';
    }
    $userParams = findUserParamsByKey($key, $db);
  }
  $query = "SELECT maps_id, name, rw_key, ro_key FROM maps WHERE users_id = $userId";
  $res = MySqlQuery($query, $db);
  if (! $res) return false;
  $mapKeys = array();
  while ($row = mysql_fetch_assoc($res)) {
    $mapKeys[] = $row;
  }
  $indexUrl = completeUrl('u', $key);
  include(TEMPLATES.'mapslist.php');
  return true;
}

class Icon {
  public $name;
  public $class;
  public $picture;

  public function __construct($name, $class, $picture) {
    $this->name = $name;
    $this->class = $class;
    $this->picture = $picture;
  }
}

function getIcons($usersId, $db) {
  $icons = array();
  $query = "SELECT name, icons_id, picture FROM icons WHERE users_id = $usersId OR is_public";
  $res = MySqlQuery($query, $db);
  if ($res) {
    while ($row = mysql_fetch_assoc($res)) {
      $icons[] = new Icon($row['name'], 'u'.$row['icons_id'], $row['picture']);
    }
  }
  return $icons;
}

function renderMapByKey($keyType, $key, $db) {
  $keyColumn = $keyType == 'w' ? 'rw_key' : 'ro_key';
  $query = "SELECT rw_key, ro_key, map, name, users_id FROM maps WHERE $keyColumn='$key'";
  $res = MySqlQuery($query, $db);
  if (! $res) return false;
  $row = mysql_fetch_assoc($res);
  if (! $row) return false;
  $startMap = strtr($row['map'], array("'" => "\'"));
  $mapName = strtr($row['name'], array('<' => '', "'" => '', '"' => ''));
  $userIcons = getIcons($row['users_id'], $db);
  include(TEMPLATES.'maps-all.php');
  return true;
}

function welcomePage($message = '') {
  setcookie('testcookie',rand());
  include(TEMPLATES.'welcome.php');
  return true;
}

function completeUrl($keyType, $key) {
  $uri = $_SERVER['REQUEST_URI'];
  if (substr($uri, -5) == '.html') {
    $uri = dirname($uri);
    if ($uri && $uri != '/') $uri .='/';
  }
  return 'https://'.$_SERVER['HTTP_HOST'].($uri ? $uri : '/').$keyType.$key.'.html';
}

function redirect($keyType, $key) {
  header('Location: '.completeUrl($keyType, $key));
}

function newUser($db) {
  $key = generateNewAccount($db);
  redirectToMapsList($key);
  return true;
}

function newMap($key, $db) {
  $userId = findUserByKey($key, $db);
  if (! $userId) return false;
  $mapKeys = storeNewMap($userId, '|||1320751886347$Nepojmenovan%C3%A1%20mapa', 'Nepojmenovaná mapa', $db);
  redirect('w', $mapKeys['rwKey']);
  return true;
}

function saveMap($key, $mapData, $db) {
  $mapParts = explode('$', $mapData);
  $mapName = mysql_real_escape_string(rawurldecode($mapParts[1]), $db);
  $mapData = mysql_real_escape_string($mapData, $db);
  $query = "UPDATE maps SET modified=NOW(), name='$mapName', map='$mapData' WHERE rw_key='$key'";
  $ret = MySqlQuery($query, $db);
  if (!$ret) return false;
  if (mysql_affected_rows($db) != 1) return false;
  return 'w'.$key;
}

function saveIcon($key, $iconId, $iconName, $iconData, $db) {
  $userId = findUserByKey($key, $db);
  $iconName = mysql_real_escape_string($iconName, $db);
  $iconData = mysql_real_escape_string($iconData, $db);
  if ($iconId) {
    $iconId = mysql_real_escape_string($iconId, $db);
    $query = "UPDATE icons SET name='$iconName', picture='$iconData' WHERE users_id='$userId' AND icons_id='$iconId'";
    $ret = MySqlQuery($query, $db);
  } else {
    $query = "INSERT icons SET name='$iconName', picture='$iconData', users_id='$userId'";
    $ret = MySqlQuery($query, $db);
    if (!$ret) return false;
    $iconId = mysql_insert_id($db);
  }
  return $iconId;
}

function redirectToMapsList($key) {
  setcookie('indexurl', 'u'.$key.'.html');
  redirect('', 'mapslist');
  return true;
}

function renderNotFound() {
  header("HTTP/1.0 404 Not Found");
  $url = 'https://'.$_SERVER['SERVER_NAME'].getScriptDirname($_SERVER['SCRIPT_NAME']).'/';
  include(TEMPLATES.'notfound.php');
}

header('Content-Type: text/html; charset=utf-8');

$db = initDatabase($DB);

$request = parseRequestUri($_SERVER['REQUEST_URI'], $_SERVER['SCRIPT_NAME']);
$indexUrl = isset($_COOKIE['indexurl']) ? parseIndexUrl($_COOKIE['indexurl']) : false;

if ($request === true) {
  if (isset($_POST['logintomaps'])) {
    loginUser($db);
  } elseif (isset($_POST['newpage'])) {
    newUser($db);
  } else {
    welcomePage();
  }
} elseif ($request){
  $key = substr($request,1);
  $keyType = $request[0];
  if (isset($_POST['savemap'])) {
    $request = saveMap($key, $_POST['mapdata'], $db);
  }
  if ($request && isset($_POST['loadmap'])) {
    redirect('u', $indexUrl);
    $request = true;
  }
  if ($request && isset($_POST['newmap'])) {
    $request = newMap($indexUrl, $db);
  }
  if ($request && isset($_POST['saveicon'])) {
    $request = saveIcon($indexUrl, $_POST['iconid'], $_POST['iconname'], $_POST['icondata'], $db);
  }
  if ($request == 'mapslist') {
    if (isset($_POST['register'])) {
      
    }
    $request = renderMapsList($indexUrl, $db);
  } else if ($request && $request !== true) {
    $key = substr($request,1);
    $keyType = $request[0];
    if ($keyType == 'r' || $keyType == 'w') {
      $request = renderMapByKey($keyType, $key, $db);
    } elseif ($keyType == 'u') {
      $request = findUserByKey($key, $db) && redirectToMapsList($key);
    }
  }
}

if (! $request) {
  renderNotFound();
}

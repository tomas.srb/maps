<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="keywords" content="dračák,dračí-doupě,drd,podzemí,mapy,editor">
    <meta name="description" content="Online editor map podzemí pro DrD a AD&amp;D.">
    <title>
      Online editor map pro Dračí doupě a AD&amp;D
    </title>
    <style>
      body { font-family: sans-serif; font-size: 12px; width: 800px; margin: auto; }
      h1 { font-size: 16px; margin: 15px 0; }
      input { border: 1px solid; border-color: darkgrey grey grey darkgrey; }
      #entryforms { display: none; }
      fieldset { text-align: center; padding: 9px; height: 145px; }
      fieldset div { margin: 5px 0; }
      fieldset div.login { margin: 15px 0 10px; }
      label { width: 60px; display: inline-block; }
      .column { width:50%; float: left; }
      .features { clear: both; padding-top: 10px; }
      #submitter { margin-top: 50px; }
      li { margin-top: 3px; }
      #message { padding: 1em 0; }
      #message span { font-weight: bold; border: solid 1px black; padding: 5px; }
    </style>
    <script>
  function Init() {
    var bn = 'new';
    var button = document.getElementById('submitter');
    button.setAttribute('type', 'submit');
    bn += 'page';
    button.setAttribute('name', bn);
    bn = 'loginto';
    var entryforms = document.getElementById('entryforms');
    entryforms.style.display = 'block';
    var login = document.getElementById('login');
    bn += 'maps';
    login.setAttribute('type', 'submit');
    login.setAttribute('name', bn);
  }
    </script>
  </head>
  <body onload="Init();">
    <h1>
      Online editor map pro Dračí doupě a AD&amp;D
    </h1>
    <noscript>
      <p>
        <strong>Pro správnou funkci editoru je nutný JavaScript. Bez JavaScriptu aplikace nedokáže ani načíst mapu.</strong>
      </p>
    </noscript>
    <div id="entryforms">
      <div class="column">
        <form method="POST">
          <fieldset>
            <legend>Přihlašovací údaje</legend>
            <?php if ($message) { ?>
              <div id="message">
                <span><?= $message ?></span>
              </div>
            <?php } ?>
            <div>
              <label for="username">Jméno:</label><input type="text" id="username" name="username" />
            </div>
            <div>
              <label for="password">Heslo:</label><input type="password" id="password" name="password"  />
            </div>
            <div class="login">
              <input id="login" type="hidden" value="Přihlášení" name="hidden" />
            </div>
            <div>
              Pokud jste nás již navštívili, použijte, prosím, přímou adresu k Vašim mapám, nebo se přihlaste
            </div>
          </fieldset>
        </form>
      </div>
      <div class="column">
        <form method="POST">
          <fieldset>
            <legend>Pokud jste zde poprvé, neváhejte a vstupte</legend>
            <input id="submitter" type="hidden" value="Vstup" name="hidden" />
            <p>
              Pro plnohodnotné užívání editoru není nutná žádná registrace. Za tlačítkem "Vstup" již na Vás čeká ukázková mapa a editor připravený k použití.
            </p>
          </fieldset>
        </form>
      </div>
    </div>
    <p class="features">
      Online aplikace umožňuje...
    </p>
    <ul>
      <li>
        <strong>Kreslit a tisknout mapy podzemí</strong> pro Dračí doupě či AD&amp;D v rastru čtverečkovaného papíru.
      </li>
      <li>
        Nakreslené mapy lze kdykoliv <strong>najít pod unikátním kódem</strong> v adrese mapy (tzv. URL).
      </li>
      <li>
        Mapu uloženou na serveru lze <strong>uložit lokálně a prohlížet lokálně</strong>, bez připojení k internetu.
      </li>
      <li>
        Ke kterémukoliv poli mapy lze <strong>vložit poznámku</strong>, která pak bude přehledně zobrazena pod mapou.
      </li>
      <li>
        Mapované podzemí může mít <strong>libovolný počet podlaží</strong> (stačí vložit příslušné schodiště do výchozího podlaží).
      </li>
      <li>
        Editor nabízí rady a nápovědy k optimálnímu využití jeho funkcí.
      </li>
      <li>
        Již při prvním vstupu je k dispozici jednoduchá vzorová mapa.
      </li>
    </ul>
    <img src="screenshot.png" alt="screenshot" title="Takhle nějak to vypadá..." />
  </body>
</html>

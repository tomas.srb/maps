<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <title>
      Online editor map pro Dračí doupě a AD&amp;D
    </title>
    <style>
      body { font-family: sans-serif; font-size: 12px; width: 800px; margin: auto; }
      h1 { font-size: 16px; margin: 15px 0; }
      table { border: 1px solid black; width: 100%; border-collapse: collapse; }
      th { border-bottom: 1px solid black; }
      th, td { padding: 4px 5px; }
      tr .url,
      tr .remove { text-align: center; }
      tr .name { text-align: left; }
      tr.map:hover { background: whitesmoke; }
      input { border: 1px solid; border-color: darkgrey grey grey darkgrey; }
      a { text-decoration: none; }
      a:hover { text-decoration: underline; }
      fieldset { text-align: center; padding: 9px; }
      fieldset div { margin: 5px 0; }
      fieldset div.register { margin: 15px 0 10px; }
      label { width: 100px; display: inline-block; text-align: left; }
      .message { padding: 1em 0; text-align: center; }
      .message span { font-weight: bold; border: solid 1px black; padding: 5px; }
      .indexurl { text-align: center; }
    </style>
    <script>
      function Remove(id) {
        var nameElement = document.getElementById('name_'+id);
        var name = nameElement.firstChild.nodeValue;
        if (confirm('Opravdu smazat mapu "'+name+'" ?')) {
          var removemap = document.getElementById('removemap');
          var remover = document.getElementById('remover');
          removemap.setAttribute('value', id);
          remover.submit();
        }
      }
    </script>
  </head>
  <body>
    <h1>Seznam odkazů na Vaše mapy</h1>
    <?php if ($tableMessage) { ?>
      <div class="message">
        <span><?= $tableMessage ?></span>
      </div>
    <?php } ?>
    <table>
      <thead>
        <tr><th class="name">Název mapy</th><th class="view url">Adresa náhledu mapy</th><th class="edit url">Adresa pro úpravy mapy</th><th>Odstranění mapy</th></tr>
      </thead>
      <tbody>
        <?php foreach($mapKeys as $map) { ?>
        <tr class="map">
          <td class="name" id="name_<?= $map['maps_id'] ?>"><?= htmlspecialchars($map['name']) ?></td>
          <td class="view url">
            <a href="r<?= $map['ro_key'] ?>.html">Náhled</a>
          </td>
          <td class="edit url">
            <a href="w<?= $map['rw_key'] ?>.html">Editovat</a>
          </td>
          <td class="remove">
            <input type="button" class="remove" value="X" onclick="Remove(<?= $map['maps_id'] ?>);" />
          </td>
        </tr>
        <?php } ?>
      <tr>
        <td class="name">
          <form method="POST"><div><input type="submit" value="Vytvořit novou mapu" name="newmap" /></div></form>
        </td>
        <td class="view url">&nbsp;</td>
        <td class="edit url">&nbsp;</td>
      </tr>
      </tbody>
    </table>
    <p>
      Výše uvedená tabulka obsahuje odkazy na Vaše mapy. 
      Ke každé mapě lze přistoupit dvěma udkazy - jedním pouze pro čtení, druhý umožňuje mapu i měnit a uložit.
      Pokud potřebujete mapu s někým sdílet, pošlete mu odkaz dle toho, jaká práva mu chcete poskytnout.
    </p>
    <div>
      Trvalým klíčem k Vašim mapám Vám může být tato adresa: 
      <p class="indexurl">
        <?= $indexUrl ?>
      </p>
      <form method="POST">
        <fieldset>
          <legend>nebo přihlášení zde zadanými údaji:</legend>
          <?php if ($message) { ?>
            <div class="message">
              <span><?= $message ?></span>
            </div>
          <?php } ?>
          <div>
            <label for "name">Jméno:</label><input type="text" id="username" name="username" value="<?= $userParams['username'] ?>" />
          </div>
          <?php if ($userParams['username']) { ?>
            <div>
              <label for "oldpassword">Původní heslo:</label><input type="password" id="oldpassword" name="oldpassword"  />
            </div>
          <?php } ?>
          <div>
            <label for "password">Heslo:</label><input type="password" id="password" name="password"  />
          </div>
          <div class="register">
            <input id="register" type="submit" value="Registrace" name="register" />
          </div>
        </fieldset>
      </form>
    </div>
    <form id="remover" method="post">
      <div>
        <input id="removemap" type="hidden" value="" name="removemap" />
      </div>
    </form>
  </body>
</html>
